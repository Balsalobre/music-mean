'use strict' /*Introducir instrucciones de los nuevos estándares JS*/

var mongoose = require('mongoose'); //Cargamos la librería
var app = require('./app');
var port = process.env.PORT || 8080;

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost:27017/music_mean', (err, res)=>{
	if(err){
		throw err;
	}else{
		console.log('La conexión a la base de datos está funcionando correctamente.');
		
		app.listen(port, function(){
			console.log("Servidor del api rest de música escuchando en http://localhost:"+port);
		});
	}
});
