'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AlbumSchema = Schema({
		createdAt: { type: Date, default: Date.now },
		title: String,
		description: String,
		year: Number,
		image: String,
		artist: { type: Schema.ObjectId, ref: 'Artist' }
});

module.exports = mongoose.model('Album', AlbumSchema);

